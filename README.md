## Installation Steps

### `git clone https://andy_hurd@bitbucket.org/andy_hurd/nextrip-andyhurd.git`

Pulls application code into your local machine

### `npm install`

Downloads and installs app dependencies.

## Available Scripts

Once the application is installed into your local machine, from inside the newly-created project directory, you can run:

### Run application: `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### Execute application tests: `npm test`

Launches the test runner in the interactive watch mode.<br />

### Build application `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

To serve locally, you may use the serve command `serve -s build`

## Development Assumptions

1. Standalone App
  - Root node currently does not take in parameters from a parent page (affects customization ability from outside our root node)
  - Since the app also includes routing this could have side effects if it were to have a parent application with its own routing mechanism
  
2. Design Liberties (and Iterative Collaboration)
  - As a developer, our first pass at a UI is absolutely never correct (a word is off, an icon isn't quite right, we don't like that color)
  - Used to aligning designs / integrating with a pattern library composed of approved styles and or base components
  - Normally, would have an iterative review with some designated 'approver' to see if a design implementation met the feature goal
  
3. Service call caching
  - Wrapped calls to the backend in a caching interceptor with small time to live (this pulled data seems fairly static, but would want to verify with api provider on how long a data point is 'fresh')
  
4. Browser Support
  - Went with the default out of the create-react-app starter script
  - For production development, I'm sure we'd align to a team / company / project standard of supported browsers
  
5. The user is accessing the page in a brower, with javascript enabled