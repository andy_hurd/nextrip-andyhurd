import axios from 'axios';
import { setupCache } from 'axios-cache-adapter'

const cacheMaxAgeSeconds = 60;
const cache = setupCache({
  maxAge: cacheMaxAgeSeconds * 1000
});

const http = axios.create({
  adapter: cache.adapter
});

export default http;