import api from './Http';

const nextTripRoot = 'https://svc.metrotransit.org/NexTrip';

const handleResponse = response => response.data;
const handleError = error => {
  throw error;
};
const get = (url) => {
  return api
    .get(url)
    .then(handleResponse)
    .catch(handleError);
};

const routeUrl = () => `${nextTripRoot}/Routes`;
const getRoutes = () => {
  return get(routeUrl());
};

const directionsUrl = (routeId) => `${nextTripRoot}/Directions/${routeId}`;
const getDirections = (routeId) => {
  return get(directionsUrl(routeId));
};

const stopsUrl = (routeId, directionId) => `${nextTripRoot}/Stops/${routeId}/${directionId}`;
const getStops = (routeId, directionId) => {
  return get(stopsUrl(routeId, directionId));
};

const service = {
  getRoutes,
  getDirections,
  getStops
};

export default service;