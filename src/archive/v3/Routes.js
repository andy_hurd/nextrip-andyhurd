import React, { useState, useEffect, Fragment } from 'react';
import service from './Service';
import {
  Link,
  useParams,
  useRouteMatch
} from "react-router-dom";

function Routes() {
  let { routeId } = useParams();
  let match = useRouteMatch();
  
  const [routes, setRoutes] = useState([]);
  const [route, setRoute] = useState(null);
  const [open, setOpen] = useState(false);

  function getRoutes() {
    service.getRoutes().then(function (data) {
      if (!route) {
        setRoute(data.filter(r => r.Route === routeId).shift());
      }
      if (JSON.stringify(data) !== JSON.stringify(routes)) {
        setRoutes(data || []);
      }
    })
  }
  
  useEffect(getRoutes);
  
  function selectRoute(route) {
    setOpen(false);
    setRoute(routes.filter(r => r.Route === route).shift());
  }
  
  const baseUrl = match.url.split(`/${routeId}/direction`)[0];
  
  return routes ? (
      <Fragment>
        <header className="App-header" onClick={() => setOpen(!open)}>
          {route ? route.Description : 'Select Route'}
        </header>
        {open ? (
          <ul>
            {routes.map(route => (
              <li key={route.Route}>
                <Link to={`${baseUrl}/${route.Route}/direction`} onClick={() => selectRoute(route.Route)}>
                  {route.Description}
                </Link>
              </li>
            ))}
          </ul>
        ): null}
      </Fragment>
    ) : null;
}

export default Routes;
