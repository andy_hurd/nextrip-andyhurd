import React, { useState, useEffect, Fragment } from 'react';
import service from './Service';
import {
  Link,
  useParams,
  useRouteMatch
} from "react-router-dom";

function Directions() {
  
  let { routeId } = useParams();
  let { directionId } = useParams();
  let match = useRouteMatch();
  
  const [directions, setDirections] = useState([]);
  const [direction, setDirection] = useState(null);
  const [open, setOpen] = useState(false);

  function getDirections() {
    service.getDirections(routeId).then(function (data) {
      if (!direction) {
        setDirection(data.filter(d => d.Value === directionId).shift());
      }
      if (JSON.stringify(data) !== JSON.stringify(directions)) {
        setDirections(data || []);
      }
    });
  }
  
  useEffect(getDirections);
  
  function selectDirection(direction) {
    setOpen(false);
    setDirection(direction);
  }
  
  const baseUrl = match.url.split(`/${directionId}/stops`)[0];
  
  if (direction && !directionId) {
    setDirection(null);
  }
  
  return (
    <Fragment>
      <header className="App-header" onClick={() => setOpen(!open)}>
        {direction ? direction.Text : 'Select Direction'}
      </header>
      {open ? (
        <ul>
          {directions.map(direction => (
            <li key={direction.Value}>
              <Link to={`${baseUrl}/${direction.Value}/stops`} onClick={() => selectDirection(direction)}>
                {direction.Text}
              </Link>
            </li>
          ))}
        </ul>
      ) : null}
    </Fragment>
  );
}

export default Directions;
