import React, { useState, useEffect, Fragment } from 'react';
import service from './Service';
import {
  useParams
} from "react-router-dom";

function Stops() {
  
  let { routeId, directionId } = useParams();
  const [stops, setStops] = useState([]);

  function getStops() {
    service.getStops(routeId, directionId).then(function (data) {
      if (JSON.stringify(data) !== JSON.stringify(stops)) {
        setStops(data || []);
      }
    });
  }
  
  useEffect(getStops);
  
  return (
    <Fragment>
      <header className="App-header">
        Stops
      </header>
      <ul>
        {stops.map(stop => (
          <li key={stop.Value}>
              {stop.Text}
          </li>
        ))}
      </ul>
    </Fragment>
  );
}

export default Stops;
