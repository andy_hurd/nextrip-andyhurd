import axios from 'axios';

const urls = {
  v1: 'https://svc.metrotransit.org/NexTrip/Routes?format=json',
  v2: 'https://svc.metrotransit.org/nextripv2/routes'
};

const directionUrls = {
  v1: 'https://svc.metrotransit.org/NexTrip/Directions/{routeId}?format=json',
  v2: 'https://svc.metrotransit.org/nextripv2/directions/{routeId}'
};

const stopsUrls = {
  v1: 'https://svc.metrotransit.org/NexTrip/Stops/{routeId}/{directionId}?format=json'
};

let routesCache;
const getRoutes = () => {
  if (routesCache) {
    return new Promise(resolve => resolve(routesCache));
  }
  return axios
    .get(urls.v1)
    .then(response => {
      routesCache = response.data;
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};

let directionsCache = {};
const getDirections = (routeId) => {
  if (directionsCache[routeId]) {
    return new Promise(resolve => resolve(directionsCache[routeId]));
  }
  const url = directionUrls.v1.replace('{routeId}', routeId);
  return axios
    .get(url)
    .then(response => {
      directionsCache[routeId] = response.data;
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};

let stopsCache = {};
const getStops = (routeId, directionId) => {
  const key = routeId + "_" + directionId;
  if (stopsCache[key]) {
    return new Promise(resolve => resolve(stopsCache[key]));
  }
  const url = stopsUrls.v1.replace('{routeId}', routeId).replace('{directionId}', directionId);
  return axios
    .get(url)
    .then(response => {
      stopsCache[key] = response.data;
      return response.data;
    })
    .catch(error => {
      throw error;
    });
};

const service = {
  getRoutes,
  getDirections,
  getStops
};

export default service;