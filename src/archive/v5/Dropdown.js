import React, { useState, Fragment } from 'react';
import {
  Link
} from "react-router-dom";
import {FaAngleUp, FaAngleDown} from 'react-icons/lib/fa';

function Dropdown(props) {
  const [open, setOpen] = useState(false);
  
  const select = item => {
    setOpen(false);
    props.onSelect(item);
  };
  
  return props.items ? (
      <Fragment>
        <div className="App-header">
          <span style={{minWidth: '110px'}}>{props.label}</span>
          <button className="App-header" style={{width: '100%', backgroundColor: '#343c4b'}} onClick={() => setOpen(!open)}>
            {props.header}
            {open ? <FaAngleUp /> : <FaAngleDown />}
          </button>
        </div>
        {open ? (
          <ul>
            {props.items.map(item => (
              <li key={props.getKey(item)}>
                <Link to={props.getLink(item)} onClick={() => select(item)}>
                  {props.getDisplay(item)}
                </Link>
              </li>
            ))}
          </ul>
        ): null}
      </Fragment>
    ) : null;
}

export default Dropdown;
