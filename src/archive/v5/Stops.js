import React, { useState, useEffect, Fragment } from 'react';
import service from './Service';
import {
  useParams
} from "react-router-dom";

function Stops() {
  
  let { routeId, directionId } = useParams();
  const [stops, setStops] = useState([]);

  const getStops = () => {
    service.getStops(routeId, directionId).then(function (data) {
      setStops(data || []);
    });
  }
  
  useEffect(getStops, [routeId, directionId]);
  
  return (
    <Fragment>
      <header className="App-header" style={{justifyContent: 'center'}}>
        Stops
      </header>
      <ul>
        {stops.map(stop => (
          <li key={stop.Value}>
              {stop.Text}
          </li>
        ))}
      </ul>
    </Fragment>
  );
}

export default Stops;
