import React, { useState, useEffect, Fragment } from 'react';
import service from './Service';
import {
  Link
} from "react-router-dom";

function Routes() {
  const [routes, setRoutes] = useState([]);

  function getRoutes() {
    service.getRoutes().then(function (data) {
      if (JSON.stringify(data) !== JSON.stringify(routes)) {
        setRoutes(data || []);
      }
    })
  }
  
  useEffect(getRoutes);
  
  return (
    <Fragment>
      <header className="App-header">
        Routes
      </header>
      <ul>
        {routes.map(route => (
          <li key={route.Route}>
            <Link to={`/route/${route.Route}/directions`}>
              {route.Description}
            </Link>
          </li>
        ))}
      </ul>
    </Fragment>
  );
}

export default Routes;
