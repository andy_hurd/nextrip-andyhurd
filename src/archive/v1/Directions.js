import React, { useState, useEffect, Fragment } from 'react';
import service from './Service';
import {
  Link,
  useParams
} from "react-router-dom";

function Directions() {
  
  let { routeId } = useParams();
  const [directions, setDirections] = useState([]);
  
  console.log(routeId);

  function getDirections() {
    service.getDirections(routeId).then(function (data) {
      if (JSON.stringify(data) !== JSON.stringify(directions)) {
        setDirections(data || []);
      }
    });
  }
  
  useEffect(getDirections);
  
  return (
    <Fragment>
      <header className="App-header">
        Directions
      </header>
      <ul>
        {directions.map(direction => (
          <li key={direction.Value}>
            <Link to={`/route/${routeId}/direction/${direction.Value}`}>
              {direction.Text}
            </Link>
          </li>
        ))}
      </ul>
    </Fragment>
  );
}

export default Directions;
