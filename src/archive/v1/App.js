import React from 'react';
import Routes from './Routes';
import Directions from './Directions';
import Stops from './Stops';
import './App.css';
import {
  Switch,
  Route
} from "react-router-dom";

function App() {
  
  return (
    <div className="App">
      <Switch>
        <Route path="/route/:routeId/direction/:directionId">
          <Stops />
        </Route>
        <Route path="/route/:routeId/directions">
          <Directions />
        </Route>
        <Route path="/">
          <Routes />
        </Route>
      </Switch>
    </div>
  );
}

export default App;