export default {
  routes: () => `/route`,
  directions: (routeId) => `/route/${routeId}/direction`,
  stops: (routeId, directionId) => `/route/${routeId}/direction/${directionId}/stops`
};