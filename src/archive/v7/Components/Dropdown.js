import React, { useState } from 'react';
import {
  Link
} from "react-router-dom";
import {FaAngleUp, FaAngleDown} from 'react-icons/lib/fa';

export default function Dropdown(props) {
  
  const {
    label,
    getDisplay,
    header,
    items,
    getKey,
    getLink,
    onSelect
  } = props;
  
  const [open, setOpen] = useState(false);
  
  const select = item => {
    setOpen(false);
    onSelect(item);
  };
  
  return items ? (
      <>
        <div className="App-header">
          <span className="App-header-label">{label}</span>
          <button className="App-header" style={{width: '100%', backgroundColor: '#343c4b'}} onClick={() => setOpen(!open)}>
            {header}
            {open ? <FaAngleUp /> : <FaAngleDown />}
          </button>
        </div>
        {open ? (
          <ul>
            {items.map(item => (
              <li key={getKey(item)}>
                <Link to={getLink(item)} onClick={() => select(item)}>
                  {getDisplay(item)}
                </Link>
              </li>
            ))}
          </ul>
        ): null}
      </>
    ) : null;
}
