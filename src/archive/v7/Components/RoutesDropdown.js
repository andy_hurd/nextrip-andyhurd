import React from 'react';
import SelectDropdown from './SelectDropdown';

export default function RoutesDropdown(props) {
  
  const {
    routes,
    route,
    selectRoute,
    routeLink
  } = props;
  
  return (
    <SelectDropdown
      label={'Route'}
      items={routes}
      selected={route}
      getKey={route => route.Route}
      getDisplay={route => route && route.Description}
      onSelect={selectRoute}
      getLink={route => routeLink(route.Route)} />
  );
}
