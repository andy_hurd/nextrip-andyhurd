import React, { useState, useEffect } from 'react';
import service from '../Service/Service';
import StopsPresentation from './StopsPresentation';

export default function Stops(props) {
  
  let { routeId, directionId } = props;
  const [stops, setStops] = useState([]);

  const getStops = () => {
    service.getStops(routeId, directionId).then(function (data) {
      setStops(data || []);
    });
  }
  
  useEffect(getStops, [routeId, directionId]);
  
  return (
    <StopsPresentation stops={stops} />
  );
}
