import React from 'react';
import SelectDropdown from './SelectDropdown';

export default function DirectionsDropdown(props) {
  
  const {
    directions,
    direction,
    selectDirection,
    directionLink
  } = props;
  
  return (
    <SelectDropdown
      label={'Direction'}
      items={directions}
      selected={direction}
      getKey={direction => direction.Value}
      getDisplay={direction => direction && direction.Text}
      onSelect={selectDirection}
      getLink={direction => directionLink(direction.Value)} />
  );
}