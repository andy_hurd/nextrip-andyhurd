import React from 'react';

export default function StopsPresentation(props) {
  const { stops } = props;
  return (
    <>
      <header className="App-header" style={{justifyContent: 'center'}}>
        Stops
      </header>
      <ul>
        {stops.map(stop => (
          <li key={stop.Value}>
              {stop.Text}
          </li>
        ))}
      </ul>
    </>
  );
}
