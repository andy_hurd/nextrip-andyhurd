import React from 'react';
import DirectionsView from './DirectionsView';
import Stops from '../Components/Stops';

export default function StopsView(props) {
  return (
    <>
      <DirectionsView {...props} />
      <Stops {...props} />
    </>
  );
}
