import {
  useParams
} from "react-router-dom";

export default function View(props) {
  let { routeId, directionId } = useParams();
  
  return props.render({routeId, directionId});
}
