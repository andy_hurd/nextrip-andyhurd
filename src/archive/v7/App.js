import React from 'react';
import View from './Structure/View';
import RoutesView from './Structure/RoutesView';
import DirectionsView from './Structure/DirectionsView';
import StopsView from './Structure/StopsView';
import './App.css';
import url from './AppRoutes';
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";
export default function App() {

  const links = {
    routeLink: url.directions,
    directionLink: url.stops
  };
  
  return (
      <Switch>
        <Route path={url.stops(':routeId', ':directionId')}>
          <View render={props => (
            <StopsView {...links} {...props} />
          )} />
        </Route>
        <Route path={url.directions(':routeId')}>
          <View render={props => (
            <DirectionsView {...links} {...props} />
          )} />
        </Route>
        <Route path={url.routes()}>
          <View render={props => (
            <RoutesView {...links} {...props} />
          )} />
        </Route>
        <Route path="/">
          <Redirect to={"/route"} />
        </Route>
      </Switch>
  );
}
