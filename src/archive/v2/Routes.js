import React, { useState, useEffect, Fragment } from 'react';
import service from './Service';
import {
  useParams,
  useRouteMatch,
  useHistory
} from "react-router-dom";

function Routes() {
  let { routeId } = useParams();
  let match = useRouteMatch();
  let history = useHistory();
  
  const [routes, setRoutes] = useState([]);
  const [route, setRoute] = useState(routeId);

  function getRoutes() {
    service.getRoutes().then(function (data) {
      if (JSON.stringify(data) !== JSON.stringify(routes)) {
        setRoutes(data || []);
      }
    })
  }
  
  useEffect(getRoutes);
  
  function selectRoute(route) {
    setRoute(route);
    const baseUrl = match.url.split(`/${routeId}/direction`)[0];
    history.push(`${baseUrl}/${route}/direction`);
  }
  
  return (
    <Fragment>
      <header className="App-header">
        Routes
      </header>
      <select onChange={e => selectRoute(e.target.value)} value={route}>
        <option>Select a route</option>
        {routes.map(route => (
          <option key={route.Route} value={route.Route}>
            {route.Description}
          </option>
        ))}
      </select>
    </Fragment>
  );
}

export default Routes;
