import React, { useState, useEffect, Fragment } from 'react';
import service from './Service';
import {
  useParams,
  useRouteMatch,
  useHistory
} from "react-router-dom";

function Directions() {
  
  let { routeId } = useParams();
  let { directionId } = useParams();
  let match = useRouteMatch();
  let history = useHistory();
  
  const [directions, setDirections] = useState([]);
  const [direction, setDirection] = useState(directionId);

  function getDirections() {
    service.getDirections(routeId).then(function (data) {
      if (JSON.stringify(data) !== JSON.stringify(directions)) {
        setDirections(data || []);
      }
    });
  }
  
  useEffect(getDirections);
  
  function selectDirection(direction) {
    setDirection(direction);
    const baseUrl = match.url.split(`/${directionId}/stops`)[0];
    history.push(`${baseUrl}/${direction}/stops`);
  }
  
  return (
    <Fragment>
      <header className="App-header">
        Directions
      </header>
      <select onChange={e => selectDirection(e.target.value)} value={direction}>
        <option>Select a direction</option>
        {directions.map(direction => (
          <option key={direction.Value} value={direction.Value}>
            {direction.Text}
          </option>
        ))}
      </select>
    </Fragment>
  );
}

export default Directions;
