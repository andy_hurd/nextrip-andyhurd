import React, { useState, useEffect } from 'react';
import service from './Service';
import Dropdown from './Dropdown';
import {
  useParams,
  useRouteMatch
} from "react-router-dom";

function Routes() {
  let { routeId } = useParams();
  let match = useRouteMatch();
  
  const [routes, setRoutes] = useState([]);
  const [route, setRoute] = useState(null);

  const getRoutes = () => {
    service.getRoutes().then((routesList) => {
      if (!route) {
        setRoute(routesList.filter(r => r.Route === routeId).shift());
      }
      setRoutes(routesList || []);
    })
  }
  
  useEffect(getRoutes, []);
  
  const baseUrl = match.url.split(`/${routeId}/direction`)[0];
  
  if (route && !routeId) {
    setRoute(null);
  }
  
  const selectRoute = route => setRoute(routes.filter(r => r.Route === route.Route).shift());
  
  return (
    <Dropdown
      label={'Route:'}
      header={route ? route.Description : 'Select Route'}
      items={routes}
      getKey={route => route.Route}
      onSelect={selectRoute}
      getLink={route => `${baseUrl}/${route.Route}/direction`}
      getDisplay={route => route.Description} />
  );
}

export default Routes;
