import React from 'react';
import DirectionsView from './DirectionsView';
import Stops from './Stops';

export default function StopsView() {
  return (
    <>
      <DirectionsView />
      <Stops />
    </>
  );
}
