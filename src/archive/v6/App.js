import React from 'react';
import RoutesView from './RoutesView';
import DirectionsView from './DirectionsView';
import StopsView from './StopsView';
import './App.css';
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

export default function App() {
  return (
    <Switch>
      <Route path="/route/:routeId/direction/:directionId/stops">
        <StopsView />
      </Route>
      <Route path="/route/:routeId/direction">
        <DirectionsView />
      </Route>
      <Route path="/route">
        <RoutesView />
      </Route>
      <Route path="/">
        <Redirect to={"/route"} />
      </Route>
    </Switch>
  );
}
