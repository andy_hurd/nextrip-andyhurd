import React from 'react';
import RoutesView from './RoutesView';
import Directions from './Directions';

export default function DirectionsView() {
  return (
    <>
      <RoutesView />
      <Directions />
    </>
  );
}
