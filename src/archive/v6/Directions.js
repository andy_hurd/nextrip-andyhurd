import React, { useState, useEffect } from 'react';
import service from './Service';
import Dropdown from './Dropdown';
import {
  useParams,
  useRouteMatch
} from "react-router-dom";

function Directions() {
  
  let { routeId } = useParams();
  let { directionId } = useParams();
  let match = useRouteMatch();
  
  const [directions, setDirections] = useState([]);
  const [direction, setDirection] = useState(null);

  const getDirections = () => {
    service.getDirections(routeId).then((data) => {
      if (!direction) {
        setDirection(data.filter(d => d.Value === directionId).shift());
      }
      setDirections(data || []);
    });
  }
  
  useEffect(getDirections, [routeId]);
  
  const selectDirection = direction => setDirection(direction);
  
  const baseUrl = match.url.split(`/${directionId}/stops`)[0];
  
  if (direction && !directionId) {
    setDirection(null);
  }
  
  return (
    <Dropdown
      label={'Direction:'}
      header={direction ? direction.Text : 'Select Direction'}
      items={directions}
      getKey={direction => direction.Value}
      onSelect={selectDirection}
      getLink={direction => `${baseUrl}/${direction.Value}/stops`}
      getDisplay={direction => direction.Text} />
  );
}

export default Directions;
