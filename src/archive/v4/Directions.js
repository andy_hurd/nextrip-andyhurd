import React, { useState, useEffect } from 'react';
import service from './Service';
import Dropdown from './Dropdown';
import {
  useParams,
  useRouteMatch
} from "react-router-dom";

function Directions() {
  
  let { routeId } = useParams();
  let { directionId } = useParams();
  let match = useRouteMatch();
  
  const [directions, setDirections] = useState([]);
  const [direction, setDirection] = useState(null);

  function getDirections() {
    service.getDirections(routeId).then(function (data) {
      if (!direction) {
        setDirection(data.filter(d => d.Value === directionId).shift());
      }
      if (JSON.stringify(data) !== JSON.stringify(directions)) {
        setDirections(data || []);
      }
    });
  }
  
  useEffect(getDirections);
  
  function selectDirection(direction) {
    setDirection(direction);
  }
  
  const baseUrl = match.url.split(`/${directionId}/stops`)[0];
  
  if (direction && !directionId) {
    setDirection(null);
  }
  
  return (
    <Dropdown
      header={direction ? `Direction: ${direction.Text}` : 'Select Direction'}
      items={directions}
      getKey={direction => direction.Value}
      onSelect={selectDirection}
      getLink={direction => `${baseUrl}/${direction.Value}/stops`}
      getDisplay={direction => direction.Text} />
  );
}

export default Directions;
