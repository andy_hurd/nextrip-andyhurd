import React from 'react';
import Routes from './Routes';
import Directions from './Directions';
import Stops from './Stops';
import './App.css';
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

export default function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/route/:routeId/direction/:directionId/stops">
          <Routes />
          <Directions />
          <Stops />
        </Route>
        <Route path="/route/:routeId/direction">
          <Routes />
          <Directions />
        </Route>
        <Route path="/route">
          <Routes />
        </Route>
        <Route path="/">
          <Redirect to={"/route"} />
        </Route>
      </Switch>
    </div>
  );
}
