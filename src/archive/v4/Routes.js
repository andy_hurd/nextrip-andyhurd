import React, { useState, useEffect } from 'react';
import service from './Service';
import Dropdown from './Dropdown';
import {
  useParams,
  useRouteMatch
} from "react-router-dom";

function Routes() {
  let { routeId } = useParams();
  let match = useRouteMatch();
  
  const [routes, setRoutes] = useState([]);
  const [route, setRoute] = useState(null);

  function getRoutes() {
    service.getRoutes().then(function (data) {
      if (!route) {
        setRoute(data.filter(r => r.Route === routeId).shift());
      }
      if (JSON.stringify(data) !== JSON.stringify(routes)) {
        setRoutes(data || []);
      }
    })
  }
  
  useEffect(getRoutes);
  
  const baseUrl = match.url.split(`/${routeId}/direction`)[0];
  
  if (route && !routeId) {
    setRoute(null);
  }
  
  function selectRoute(route) {
    setRoute(routes.filter(r => r.Route === route.Route).shift());
  }
  
  return (
    <Dropdown
      header={route ? `Route: ${route.Description}` : 'Select Route'}
      items={routes}
      getKey={route => route.Route}
      onSelect={selectRoute}
      getLink={route => `${baseUrl}/${route.Route}/direction`}
      getDisplay={route => route.Description} />
  );
}

export default Routes;
