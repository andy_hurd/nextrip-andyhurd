import React, { useState, Fragment } from 'react';
import {
  Link
} from "react-router-dom";
import {FaAngleUp, FaAngleDown} from 'react-icons/lib/fa';

function Dropdown(props) {
  const [open, setOpen] = useState(false);
  
  function select(item) {
    setOpen(false);
    props.onSelect(item);
  }
  
  return props.items ? (
      <Fragment>
        <button className="App-header" onClick={() => setOpen(!open)}>
          {props.header}
          {open ? <FaAngleUp /> : <FaAngleDown />}
        </button>
        {open ? (
          <ul>
            {props.items.map(item => (
              <li key={props.getKey(item)}>
                <Link to={props.getLink(item)} onClick={() => select(item)}>
                  {props.getDisplay(item)}
                </Link>
              </li>
            ))}
          </ul>
        ): null}
      </Fragment>
    ) : null;
}

export default Dropdown;
