import React, { useState } from 'react';
import {
  Link
} from "react-router-dom";
import {FaAngleUp, FaAngleDown} from 'react-icons/lib/fa';
import styles from './Dropdown.module.scss';

export default function Dropdown(props) {
  
  const {
    label,
    getDisplay,
    header,
    items,
    getKey,
    getLink,
    onSelect
  } = props;
  
  const [open, setOpen] = useState(false);
  
  const select = item => {
    setOpen(false);
    onSelect(item);
  };
  
  return items ? (
      <nav aria-labelledby="nav-label">
        <div className={styles.dropdownHeader}>
          <span id="nav-label" className={styles.dropdownHeaderLabel}>{label}</span>
          <button aria-expanded={open} aria-controls="nav-menu-list" className={styles.dropdownHeader + ' '  + styles.dropdownHeaderButton} onClick={() => setOpen(!open)}>
            {header}
            {open ? <FaAngleUp /> : <FaAngleDown />}
          </button>
        </div>
        {open ? (
          <ul id="nav-menu-list" className={styles.dropdownList} role="menu">
            {items.map(item => (
              <li key={getKey(item)} role="menuitem">
                <Link to={getLink(item)} onClick={() => select(item)}>
                  {getDisplay(item)}
                </Link>
              </li>
            ))}
          </ul>
        ): null}
      </nav>
    ) : null;
}
