import React from "react";
import { render } from '@testing-library/react';

import SelectDropdown from "./SelectDropdown";

jest.mock("./Dropdown", () => {
  return function Dropdown(props) {
    const dummyItem = {
      itemProp1: 'capybara',
      itemProp2: 'hamster'
    };
    return (
      <div id="dropdown" className="root-el">
        <div id="dropdown-label">{props.label}</div>
        <div id="dropdown-getDisplay">{props.getDisplay(dummyItem)}</div>
        <div id="dropdown-header">{props.header}</div>
        <div id="dropdown-items">{props.items}</div>
        <div id="dropdown-getKey">{props.getKey(dummyItem)}</div>
        <div id="dropdown-getLink">{props.getLink(dummyItem)}</div>
        <div id="dropdown-onSelect">{props.onSelect}</div>
      </div>
    );
  };
});

test("it should render a Dropdown child", () => {
  const props = {
    label: 'jerboa',
    getDisplay: (selected) => `**_${selected.itemProp1}_**`,
    selected: {itemProp1: 'chipmunk', itemProp2: 'opossum'},
    items: 'RATS',
    getKey: (item) => item.itemProp1,
    getLink: (item) => item.itemProp2,
    onSelect: 'pika'
  };
  const { container } = render(
    <SelectDropdown {...props} />
  );
  expect(container.querySelector("#dropdown").childElementCount).toEqual(7);
  expect(container.querySelector("#dropdown-label").innerHTML).toEqual('jerboa');
  expect(container.querySelector("#dropdown-getDisplay").innerHTML).toEqual('**_capybara_**');
  expect(container.querySelector("#dropdown-header").innerHTML).toEqual('**_chipmunk_**');
  expect(container.querySelector("#dropdown-items").innerHTML).toEqual('RATS');
  expect(container.querySelector("#dropdown-getKey").innerHTML).toEqual('capybara');
  expect(container.querySelector("#dropdown-getLink").innerHTML).toEqual('hamster');
  expect(container.querySelector("#dropdown-onSelect").innerHTML).toEqual('pika');
});