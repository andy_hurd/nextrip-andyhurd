import React, { useState, useEffect } from 'react';
import service from '../Service/Service';
import DirectionsDropdown from './DirectionsDropdown';

export default function Directions(props) {
  
  let { routeId, directionId, directionLink } = props;
  
  const [directions, setDirections] = useState([]);
  const [direction, setDirection] = useState(null);

  const getDirections = () => {
    service.getDirections(routeId).then((data) => {
      if (!direction) {
        setDirection(data.filter(d => d.Value === directionId).shift());
      }
      setDirections(data || []);
    });
  }
  
  useEffect(getDirections, [routeId]);
  
  const selectDirection = direction => setDirection(direction);
  
  if (direction) {
    if (!directionId) {
      setDirection(null);
    } else if (directions.length && direction.Value !== directionId) {
      setDirection(directions.filter(d => d.Value === directionId).shift())
    }
  }
  
  return (
    <DirectionsDropdown
      directions={directions}
      direction={direction}
      selectDirection={selectDirection}
      directionLink={directionId => directionLink(routeId, directionId)} />
  );
}
