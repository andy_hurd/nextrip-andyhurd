import React, { useState, useEffect } from 'react';
import service from '../Service/Service';
import RoutesDropdown from './RoutesDropdown';

export default function Routes(props) {
  let { routeId, routeLink } = props;
  
  const [routes, setRoutes] = useState([]);
  const [route, setRoute] = useState(null);

  const getRoutes = () => {
    service.getRoutes().then((routesList) => {
      if (!route) {
        setRoute(routesList.filter(r => r.Route === routeId).shift());
      }
      setRoutes(routesList || []);
    })
  }
  
  useEffect(getRoutes, []);
  
  if (route && !routeId) {
    if (!routeId) {
      setRoute(null);
    } else if (routes.length && route.Route !== routeId) {
      setRoute(routes.filter(r => r.Route === routeId).shift())
    }
  }
  
  const selectRoute = route => setRoute(routes.filter(r => r.Route === route.Route).shift());
  
  return (
    <RoutesDropdown
      routes={routes}
      route={route}
      selectRoute={selectRoute}
      routeLink={routeLink} />
  );
}
