import React from "react";
import { render } from '@testing-library/react';

import StopsPresentation from "./StopsPresentation";

test("it should render empty stops list", () => {
  const stops = [];
  const { container } = render(
    <StopsPresentation stops={stops} />
  );
  expect(container.querySelector("header").textContent).toEqual('Stops');
  expect(container.querySelectorAll("li").length).toEqual(0);
});

test("it should render populated stops list", () => {
  const stops = [{Value: 'walrus', Text: 'giraffe'}, {Value: 'penguin', Text: 'hamster'}];
  const { container } = render(
    <StopsPresentation stops={stops} />
  );
  expect(container.querySelector("header").textContent).toEqual('Stops');
  expect(container.querySelectorAll("li").length).toEqual(2);
  expect(container.querySelectorAll("li")[0].textContent).toEqual('giraffe');
  expect(container.querySelectorAll("li")[1].textContent).toEqual('hamster');
});