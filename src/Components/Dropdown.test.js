import React from "react";
import { render } from '@testing-library/react';
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";

import Dropdown from "./Dropdown";

const sel = {
  label: container => container.querySelector('nav > div > span'),
  button: container => container.querySelector('nav > div > button'),
  listItems: container => container.querySelectorAll('li'),
  listItemLinks: container => container.querySelectorAll('li a')
}

test("it should render a SelectDropdown child", () => {
  const history = createMemoryHistory();
  history.push('/');
  let selected;
  const props = {
    label: 'eel',
    getDisplay: (item) => `${item.prop1}_${item.prop2}`,
    header: 'urchin',
    items: [{prop1: 'manatee', prop2: 'seal'}, {prop1: 'parrot', prop2: 'toucan'}],
    getKey: (item) => `_key_${item.prop2}`,
    getLink: (item) => `/_link_${item.prop1}`,
    onSelect: (item) => {selected = `_selected_${item.prop2}`}
  };
  const { container } = render(
    <Router history={history}>
      <Dropdown {...props} />
    </Router>
  );
  expect(history.location.pathname).toEqual('/');
  expect(sel.label(container).textContent).toEqual(props.label);
  expect(sel.button(container).textContent).toEqual(props.header);
  expect(sel.listItems(container).length).toEqual(0);
  
  // click expand button
  sel.button(container).dispatchEvent(new MouseEvent('click', {bubbles: true}));
  expect(sel.listItems(container).length).toEqual(2);
  expect(history.location.pathname).toEqual('/');
  
  // click collapse button
  sel.button(container).dispatchEvent(new MouseEvent('click', {bubbles: true}));
  expect(sel.listItems(container).length).toEqual(0);
  expect(history.location.pathname).toEqual('/');
  
  // click expand button
  sel.button(container).dispatchEvent(new MouseEvent('click', {bubbles: true}));
  expect(sel.listItems(container).length).toEqual(2);
  expect(history.location.pathname).toEqual('/');
  
  // click list item link
  sel.listItemLinks(container)[0].dispatchEvent(new MouseEvent('click', {bubbles: true}));
  expect(sel.listItems(container).length).toEqual(0);
  expect(history.location.pathname).toEqual('/_link_manatee');
  expect(selected).toEqual('_selected_seal');
  
  // click expand button
  sel.button(container).dispatchEvent(new MouseEvent('click', {bubbles: true}));
  expect(sel.listItems(container).length).toEqual(2);
  expect(history.location.pathname).toEqual('/_link_manatee');
  
  // click collapse button
  sel.button(container).dispatchEvent(new MouseEvent('click', {bubbles: true}));
  expect(sel.listItems(container).length).toEqual(0);
  expect(history.location.pathname).toEqual('/_link_manatee');
});