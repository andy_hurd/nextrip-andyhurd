import React from 'react';
import Dropdown from './Dropdown';

export default function SelectDropdown(props) {
  
  const {
    label,
    getDisplay,
    selected
  } = props;
  
  return (
    <Dropdown
      label={`${label}:`}
      header={getDisplay(selected) ||  `Select ${label}`}
      {...props} />
  );
}
