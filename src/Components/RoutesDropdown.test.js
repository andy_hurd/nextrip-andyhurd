import React from "react";
import { render } from '@testing-library/react';

import RoutesDropdown from "./RoutesDropdown";

jest.mock("./SelectDropdown", () => {
  return function SelectDropdown(props) {
    const dummyRoute = {
      Route: 'capybara',
      Description: 'hamster'
    };
    return (
      <div id="select-dropdown" className="root-el">
        <div id="select-dropdown-label">{props.label}</div>
        <div id="select-dropdown-getDisplay">{props.getDisplay(dummyRoute)}</div>
        <div id="select-dropdown-selected">{props.selected}</div>
        <div id="select-dropdown-items">{props.items}</div>
        <div id="select-dropdown-getKey">{props.getKey(dummyRoute)}</div>
        <div id="select-dropdown-getLink">{props.getLink(dummyRoute)}</div>
        <div id="select-dropdown-onSelect">{props.onSelect}</div>
      </div>
    );
  };
});

test("it should render a SelectDropdown child", () => {
  const props = {
    route: 'jerboa',
    routes: 'pika',
    selectRoute: 'chipmunk',
    routeLink: (routeId) => `${routeId}_squirrel`
  };
  const { container } = render(
    <RoutesDropdown {...props} />
  );
  expect(container.querySelector("#select-dropdown").childElementCount).toEqual(7);
  expect(container.querySelector("#select-dropdown-label").innerHTML).toEqual('Route');
  expect(container.querySelector("#select-dropdown-getDisplay").innerHTML).toEqual('hamster');
  expect(container.querySelector("#select-dropdown-selected").innerHTML).toEqual('jerboa');
  expect(container.querySelector("#select-dropdown-items").innerHTML).toEqual('pika');
  expect(container.querySelector("#select-dropdown-getKey").innerHTML).toEqual('capybara');
  expect(container.querySelector("#select-dropdown-getLink").innerHTML).toEqual('capybara_squirrel');
  expect(container.querySelector("#select-dropdown-onSelect").innerHTML).toEqual('chipmunk');
});