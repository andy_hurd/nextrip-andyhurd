import React from "react";
import { render } from '@testing-library/react';

import DirectionsDropdown from "./DirectionsDropdown";

jest.mock("./SelectDropdown", () => {
  return function SelectDropdown(props) {
    const dummyDirection = {
      Value: 'capybara',
      Text: 'hamster'
    };
    return (
      <div id="select-dropdown" className="root-el">
        <div id="select-dropdown-label">{props.label}</div>
        <div id="select-dropdown-getDisplay">{props.getDisplay(dummyDirection)}</div>
        <div id="select-dropdown-selected">{props.selected}</div>
        <div id="select-dropdown-items">{props.items}</div>
        <div id="select-dropdown-getKey">{props.getKey(dummyDirection)}</div>
        <div id="select-dropdown-getLink">{props.getLink(dummyDirection)}</div>
        <div id="select-dropdown-onSelect">{props.onSelect}</div>
      </div>
    );
  };
});

test("it should render a SelectDropdown child", () => {
  const props = {
    direction: 'jerboa',
    directions: 'pika',
    selectDirection: 'chipmunk',
    directionLink: (directionValue) => `${directionValue}_squirrel`
  };
  const { container } = render(
    <DirectionsDropdown {...props} />
  );
  expect(container.querySelector("#select-dropdown").childElementCount).toEqual(7);
  expect(container.querySelector("#select-dropdown-label").innerHTML).toEqual('Direction');
  expect(container.querySelector("#select-dropdown-getDisplay").innerHTML).toEqual('hamster');
  expect(container.querySelector("#select-dropdown-selected").innerHTML).toEqual('jerboa');
  expect(container.querySelector("#select-dropdown-items").innerHTML).toEqual('pika');
  expect(container.querySelector("#select-dropdown-getKey").innerHTML).toEqual('capybara');
  expect(container.querySelector("#select-dropdown-getLink").innerHTML).toEqual('capybara_squirrel');
  expect(container.querySelector("#select-dropdown-onSelect").innerHTML).toEqual('chipmunk');
});