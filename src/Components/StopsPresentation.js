import React from 'react';
import styles from './StopsPresentation.module.scss';

export default function StopsPresentation(props) {
  const { stops } = props;
  return (
    <section>
      <header className={styles.stopsPresentationHeader}>
        Stops
      </header>
      <ul className={styles.stopsPresentationList}>
        {stops.map(stop => (
          <li key={stop.Value}>
              {stop.Text}
          </li>
        ))}
      </ul>
    </section>
  );
}
