import React from "react";
import { render } from '@testing-library/react';

import DirectionsView from "./DirectionsView";

jest.mock("./RoutesView", () => {
  return function DummyRoutesView(props) {
    return (
      <div id="routes-view" className="root-el">
        <div id="routes-view-name">RoutesView</div>
        <div id="routes-view-routeId">{props.routeId}</div>
        <div id="routes-view-routeLink">{props.routeLink}</div>
      </div>
    );
  };
});

jest.mock("../Components/Directions", () => {
  return function DummyDirections(props) {
    return (
      <div id="directions" className="root-el">
        <div id="directions-name">Directions</div>
        <div id="directions-routeId">{props.routeId}</div>
        <div id="directions-directionId">{props.directionId}</div>
        <div id="directions-directionLink">{props.directionLink}</div>
      </div>
    );
  };
});

test("it should render 2 children", () => {
  const props = {
    routeId: 'hello',
    directionId: 'from',
    routeLink: 'out',
    directionLink: 'here'
  };
  const { container } = render(
    <DirectionsView {...props} />
  );
  expect(container.querySelectorAll(".root-el").length).toEqual(2);
});

test("it should render a RoutesView child", () => {
  const props = {
    routeId: 'wutthe',
    routeLink: 'heck'
  };
  const { container } = render(
    <DirectionsView {...props} />
  );
  expect(container.querySelector("#routes-view").childElementCount).toEqual(3);
  expect(container.querySelector("#routes-view-name").innerHTML).toEqual('RoutesView');
  expect(container.querySelector("#routes-view-routeId").innerHTML).toEqual(props.routeId);
  expect(container.querySelector("#routes-view-routeLink").innerHTML).toEqual(props.routeLink);
});

test("it should render a Directions child", () => {
  const props = {
    routeId: 'mar',
    directionId: 'mo',
    directionLink: 'set'
  };
  const { container } = render(
    <DirectionsView {...props} />
  );
  expect(container.querySelector("#directions").childElementCount).toEqual(4);
  expect(container.querySelector("#directions-name").innerHTML).toEqual('Directions');
  expect(container.querySelector("#directions-routeId").innerHTML).toEqual(props.routeId);
  expect(container.querySelector("#directions-directionId").innerHTML).toEqual(props.directionId);
  expect(container.querySelector("#directions-directionLink").innerHTML).toEqual(props.directionLink);
});