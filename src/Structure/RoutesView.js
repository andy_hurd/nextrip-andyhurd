import React from 'react';
import Routes from '../Components/Routes';

export default function RoutesView(props) {
  
  return (
    <Routes {...props} />
  );
}
