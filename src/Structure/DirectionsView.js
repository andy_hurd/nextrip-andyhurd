import React from 'react';
import RoutesView from './RoutesView';
import Directions from '../Components/Directions';

export default function DirectionsView(props) {
  return (
    <>
      <RoutesView {...props} />
      <Directions {...props} />
    </>
  );
}
