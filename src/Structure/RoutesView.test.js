import React from "react";
import { render } from '@testing-library/react';

import RoutesView from "./RoutesView";

jest.mock("../Components/Routes", () => {
  return function DummyRoutes(props) {
    return (
      <div id="routes" className="root-el">
        <div id="routes-name">Routes</div>
        <div id="routes-routeId">{props.routeId}</div>
        <div id="routes-routeLink">{props.routeLink}</div>
      </div>
    );
  };
});

test("it should render 1 child", () => {
  const props = {
    routeId: 'Doggo',
    routeLink: 'Gato'
  };
  const { container } = render(
    <RoutesView {...props} />
  );
  expect(container.querySelectorAll(".root-el").length).toEqual(1);
});

test("it should render a Routes child", () => {
  const props = {
    routeId: 'Ollie',
    routeLink: 'Zissou'
  };
  const { container } = render(
    <RoutesView {...props} />
  );
  expect(container.querySelector("#routes").childElementCount).toEqual(3);
  expect(container.querySelector("#routes-name").innerHTML).toEqual('Routes');
  expect(container.querySelector("#routes-routeId").innerHTML).toEqual(props.routeId);
  expect(container.querySelector("#routes-routeLink").innerHTML).toEqual(props.routeLink);
});