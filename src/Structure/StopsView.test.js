import React from "react";
import { render } from '@testing-library/react';

import StopsView from "./StopsView";

jest.mock("./DirectionsView", () => {
  return function DummyDirectionsView(props) {
    return (
      <div id="directions-view" className="root-el">
        <div id="directions-view-name">DirectionsView</div>
        <div id="directions-view-routeId">{props.routeId}</div>
        <div id="directions-view-directionId">{props.directionId}</div>
        <div id="directions-view-routeLink">{props.routeLink}</div>
        <div id="directions-view-directionLink">{props.directionLink}</div>
      </div>
    );
  };
});

jest.mock("../Components/Stops", () => {
  return function DummyStops(props) {
    return (
      <div id="stops" className="root-el">
        <div id="stops-name">Stops</div>
        <div id="stops-routeId">{props.routeId}</div>
        <div id="stops-directionId">{props.directionId}</div>
        <div id="stops-routeLink">{props.routeLink}</div>
        <div id="stops-directionLink">{props.directionLink}</div>
      </div>
    );
  };
});

test("it should render 2 children", () => {
  const props = {
    routeId: 'hello',
    directionId: 'from',
    routeLink: 'out',
    directionLink: 'here'
  };
  const { container } = render(
    <StopsView {...props} />
  );
  expect(container.querySelectorAll(".root-el").length).toEqual(2);
});

test("it should render a DirectionsView child", () => {
  const props = {
    routeId: 'tuna',
    directionId: 'buffalo',
    routeLink: 'doggo',
    directionLink: 'kiwi'
  };
  const { container } = render(
    <StopsView {...props} />
  );
  expect(container.querySelector("#directions-view").childElementCount).toEqual(5);
  expect(container.querySelector("#directions-view-name").innerHTML).toEqual('DirectionsView');
  expect(container.querySelector("#directions-view-routeId").innerHTML).toEqual(props.routeId);
  expect(container.querySelector("#directions-view-directionId").innerHTML).toEqual(props.directionId);
  expect(container.querySelector("#directions-view-routeLink").innerHTML).toEqual(props.routeLink);
  expect(container.querySelector("#directions-view-directionLink").innerHTML).toEqual(props.directionLink);
});

test("it should render a Stops child", () => {
  const props = {
    routeId: 'penguin',
    directionId: 'marmot',
    routeLink: 'hamster',
    directionLink: 'pteradactyl'
  };
  const { container } = render(
    <StopsView {...props} />
  );
  expect(container.querySelector("#stops").childElementCount).toEqual(5);
  expect(container.querySelector("#stops-name").innerHTML).toEqual('Stops');
  expect(container.querySelector("#stops-routeId").innerHTML).toEqual(props.routeId);
  expect(container.querySelector("#stops-directionId").innerHTML).toEqual(props.directionId);
  expect(container.querySelector("#stops-routeLink").innerHTML).toEqual(props.routeLink);
  expect(container.querySelector("#stops-directionLink").innerHTML).toEqual(props.directionLink);
});