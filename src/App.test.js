import React from "react";
import { render } from '@testing-library/react';

import App from "./App";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";
import url from './AppRoutes';

jest.mock("./Structure/RoutesView", () => {
  return function DummyRoutesView(props) {
    return (
      <div>RoutesView_{props.routeId}_{props.directionId}_{props.routeLink('walrus')}</div>
    );
  };
});

jest.mock("./Structure/DirectionsView", () => {
  return function DummyDirectionsView(props) {
    return (
      <div>DirectionsView_{props.routeId}_{props.directionId}_{props.routeLink('walrus')}_{props.directionLink('walrus', 'giraffe')}</div>
    );
  };
});

jest.mock("./Structure/StopsView", () => {
  return function DummyStopsView(props) {
    return (
      <div>StopsView_{props.routeId}_{props.directionId}_{props.routeLink('walrus')}_{props.directionLink('walrus', 'giraffe')}</div>
    );
  };
});

const routesLink = url.routes();
const walrusRouteLink = url.directions('walrus');
const walrusGiraffeDirectionLink = url.stops('walrus', 'giraffe');

const assertRoutesView = (container, history) => {
  expect(container.innerHTML).toEqual(`<div>RoutesView___${walrusRouteLink}</div>`);
  expect(history.location.pathname).toEqual(routesLink);
};

const assertDirectionsView = (container, history) => {
  expect(container.innerHTML).toEqual(`<div>DirectionsView_walrus__${walrusRouteLink}_${walrusGiraffeDirectionLink}</div>`);
  expect(history.location.pathname).toEqual(walrusRouteLink);
};

const assertStopsView = (container, history) => {
  expect(container.innerHTML).toEqual(`<div>StopsView_walrus_giraffe_${walrusRouteLink}_${walrusGiraffeDirectionLink}</div>`);
  expect(history.location.pathname).toEqual(walrusGiraffeDirectionLink);
};

test("it should redirect to '/routes' on a request to '/'", () => {
  const history = createMemoryHistory();
  history.push('/');
  const { container } = render(
    <Router history={history}>
      <App />
    </Router>
  );
  assertRoutesView(container, history);
});

test("it should render the RoutesView on '/routes'", () => {
  const history = createMemoryHistory();
  history.push('/');
  const { container } = render(
    <Router history={history}>
      <App />
    </Router>
  );
  assertRoutesView(container, history);
  history.push(routesLink);
  assertRoutesView(container, history);
});

test("it should render the DirectionsView on '/routes/:routeId/direction'", () => {
  const history = createMemoryHistory();
  history.push('/');
  const { container } = render(
    <Router history={history}>
      <App />
    </Router>
  );
  assertRoutesView(container, history);
  history.push(walrusRouteLink);
  assertDirectionsView(container, history);
});

test("it should render the StopsView on '/routes/:routeId/direction/:directionId/stops'", () => {
  const history = createMemoryHistory();
  history.push('/');
  const { container } = render(
    <Router history={history}>
      <App />
    </Router>
  );
  assertRoutesView(container, history);
  history.push(walrusGiraffeDirectionLink);
  assertStopsView(container, history);
});