import React from 'react';
//import App from './archive/v1/App';
//import App from './archive/v2/App';
//import App from './archive/v3/App';
//import App from './archive/v4/App';
//import App from './archive/v5/App';
//import App from './archive/v6/App';
//import App from './archive/v7/App';
import App from './App';
import { BrowserRouter as Router } from "react-router-dom";

export default function AppRoot() {
  return (
    <Router>
      <App />
    </Router>
  );
}